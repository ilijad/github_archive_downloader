import os
import requests


hours = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19',
         '20', '21', '22', '23']
days = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30']
months = ['01','02','03','04','05','06','07','08','09','10','11','12']
years = ['2015','2016','2017','2018','2019']

BASE_URL = "https://data.gharchive.org/"
DOWNLOAD_BASE_LOCATION = "/github_archive/"

def deleteParsedArchive(dowload_directory,file_name):
    file = dowload_directory + file_name
    os.system("rm " + file)
    print("DELETED PARSED FILE----------------->    " + file)

def parseUniqueEmailAddreses(dowload_directory, file_name, date):
    file = dowload_directory + file_name
    file_final = '"%s"' % file
    os.system("zgrep -oh " + '"\w*@\w*.com"' + " " + file_final + " | sort | uniq " + " > " + dowload_directory + "emails_" + date + ".txt" )
    print("FINISHED PARSING EMAILS----------------->  " +  dowload_directory + "emails_" + date + ".txt")


def parseUniqueUsers(dowload_directory, file_name, date):
    file = dowload_directory + file_name
    file_final = '"%s"' % file
    os.system(
        "zgrep -oh " + '"https://api.github.com/users/\w*/"' + " " + file_final + " | sort | uniq " + " > " + dowload_directory + "usernames_" + date + ".txt")
    print("FINISHED PARSING USERNAMES----------------->  " + dowload_directory + "api_usernames_" + date + ".txt")


def createDownloadFolder(download_directory):
    os.makedirs(download_directory)
    print("Created directory: " + download_directory)


def downloadGzippedArchive(download_url, dowload_directory, file_name):
    response = requests.get(download_url, stream=True)
    if response.status_code == 200:
        with open(dowload_directory + file_name, 'wb') as f:
            f.write(response.raw.read())
            f.close()
    print("Downloaded Gzipped Archive: ----->  " + dowload_directory + file_name)


def parseGzippedFile(dowload_directory, file_name, date):
    parseUniqueEmailAddreses(dowload_directory, file_name, date)
    parseUniqueUsers(dowload_directory, file_name, date)
    deleteParsedArchive(dowload_directory,file_name)
    print("FINISHED PARSING GZIP----------------->  " + dowload_directory + file_name)


for year in years:
    for month in months:
        for day in days:
            DOWNLOAD_DIRECTORY = DOWNLOAD_BASE_LOCATION + year + "/" + month + "/" + day + "/"
            createDownloadFolder(DOWNLOAD_DIRECTORY)
        for hour in hours:
            FILE_NAME = year + "-" + month + "-" + day + "-" + hour + ".json.gz"
            DATE = year + "-" + month + "-" + day + "-" + hour
            DOWNLOAD_URL = BASE_URL + FILE_NAME
            DOWNLOAD_DIRECTORY = DOWNLOAD_BASE_LOCATION + year + "/" + month + "/" + day + "/"
            downloadGzippedArchive(DOWNLOAD_URL, DOWNLOAD_DIRECTORY, FILE_NAME)
            parseGzippedFile(DOWNLOAD_DIRECTORY, FILE_NAME, DATE)


#TODO Add Safty mechanism and check for the existance of folders
#TODO Create Sync Mechanism so that the script can take the latest available actions
#TODO Create Extension to gather the usernames and find their social profiles.