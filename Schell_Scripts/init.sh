#!/bin/bash

#Start python script and create list of download commands
echo "Starting Process......."
python github_archive_downloader.py  > wget_commands.txt
echo "Starting to download specified files"
cat wget_commands.txt | bash

echo "___________________________Finished downloading___________________________"

echo "Starting to parse the downloaded archives"
bash file_parser.sh

