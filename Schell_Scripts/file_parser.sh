#!/bin/bash

echo "Finding and storing all users in the downloadev archives"
find /github_archive/ -name "*.json.gz" | xargs zgrep -oh "https://api.github.com/users/\w*/" > usernames.txt
echo "Sorting and removing duplicate user entries"
sort usernames.txt | uniq >> unique_usernames.txt
rm usernames.txt

echo "Finding and storing all unique email addresses from the archives"
find /github_archive/ -name "*.json.gz" | xargs zgrep -oh "" >> emails.txt
sort emails.txt | uniq >> unique_emails.txt
rm emails.txt

echo "Calling email parser script"
bash email_parser.sh