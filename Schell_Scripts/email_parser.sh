#!/bin/bash

echo "Starting to find unique_emails"
find /github_archive/ -name "*.json.gz" | xargs zgrep -oh "\w*@\w*.com" >> emails.txt
sort emails.txt | uniq >> unique_emails.txt
rm emails.txt
echo "FINISHEd ------> CHECK UNIQUE_EMAILS.TXT"


echo "----------------------------------------------------------------------------------------"
echo "                                 RUNNING CLEANUP SCRIPT"
rm -rf /github_archive/
echo "----------------------------------------------------------------------------------------"


echo "____________________________________PROCESS FINISHEd____________________________________\n"

echo "----------------------------------------------------------------------------------------"
echo "Output files:"
echo "  - unique_emails.txt"
echo "  - unique_usernames.txt"
echo "----------------------------------------------------------------------------------------"


